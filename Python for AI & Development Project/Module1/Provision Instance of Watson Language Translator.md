## Provision Instance of Watson Language Translator (10 min)

To complete the project in this course you will require an instance of Watson Language Translator service. You should have created this service in the Python Basics for Data Science course.  However, if you no longer have an instance please follow the instructions in the lab below (refer to Step 6): 

[Lab: Instructions for Speech to Text and Language Translator service](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-PY0101EN-SkillsNetwork/labs/Module%205/PY0101_Module5_Instructions_for_Speech_to_Text_and_Language_Translator_API_Keys.md.html)
