## Project Overview: Scenario

In this project, you will be using the IBM Watson APIs to create two language translators. English to French and English to German. You will be writing unit tests to test the functionality of the code you have written. You will follow the python coding conventions, and verify them by running static code analysis. Finally, you will be creating a python package out of the files you have created for easy delivery to your clients. You will be evaluated based on the following factors. 

- The code you have written. 

- The unit tests you have written. 

- Unit test results. 

- Static code analysis results.  

You will be evaluated by your peers and will be awarded points for the completeness of your code, tests and the analysis results as evidenced by your screenshots which you save as you work through the project and upload in the peer review assignment.

You will be using the Theia IDE to do this lab. When you log into the Theia environment, you are presented with a 'dedicated computer on the cloud' exclusively for you. This is available to you as long as you work on the labs. **Once you log off, this 'dedicated computer on the cloud' is deleted along with any files you may have created. So, it is a good idea to finish your assignment in a single session.**


If you finish part of the assignment and return to the Theia lab later, you may have to start from the beginning. Plan to finish out the assignment when you have the time to finish the complete assignment in a single session. Alternatively, you can also save your work as you go into GitHub or save your code locally on your computer. 
