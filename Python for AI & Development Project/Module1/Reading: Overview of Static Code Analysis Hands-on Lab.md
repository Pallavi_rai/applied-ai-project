# Reading: Overview of Static Code Analysis Hands-on Lab (1 min)

### Overview of Static Code Analysis Hands-on Lab

Static Code Analysis is an important step in creating python code which is compliant with the standards.  This hands-on lab will show you how to install pylint and run Static Code Analysis on a python program.  You will check the compliance score of that program and be able to fix common mistakes to improve the compliance score.

As a refresher, you can access the [Python Style Guide](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-PY0222EN-SkillsNetwork/labs/module%206/Reading%20-%20Python%20Style%20Guide/PythonStyleGuide.md.html) in a new tab.
