## Finding new clients locally and online

At some point, you'll have the skills to build chatbots and perhaps even have a fancy presentation for prospective clients. Off to a great start. But how do you go about finding clients? Maybe you are just a student in a dorm room somewhere. Can you really make a business out of this? Absolutely!

There are two roads you can take (though you could do both if you are ambitious):


1. Sell locally to businesses near you;

2. Sell remotely to a global audience via the internet.

### Selling your services locally

Selling locally means reaching out to businesses in your city or region. The key advantage is that you could literally knock on the door of local businesses and see if they are interested in what you have to offer.

You can also email or phone them, and see if they are interested in a quick no-string-attached presentation on the services you can offer them. Not to mention that you can post your offer in local classified ads.

When approaching clients, even in this preliminary stages, always lead with the value you can provide. It's not about what you can do per se, it's about how the client can benefit from it.

In-person meetings will also enable you to discuss requirements much more easily. The downside is that if you live in a small town or somewhat rather rural, local businesses might be less inclined to invest in high tech solutions like chatbots.

Thankfully, if the local option doesn't work too well for you, you still have the opportunity to sell online.

#### Selling your services online

The great thing about selling online is that you can do it in your pajamas and in your spare time. So it's a very flexible option, even if you already have various commitments like school or taking care of your children at home.

The same principles covered in the previous section apply. The only difference is that you'll typically want to sign up with various freelancing sites to find clients.

A few sites worth considering are:


- [Fiverr](https://www.fiverr.com/)

- [Upwork](https://www.upwork.com/)

- [Freelancer](https://www.freelancer.com/)

- [Guru](https://www.guru.com/)

- [People Per Hour](https://www.peopleperhour.com/)

- [Flexjobs](https://www.flexjobs.com/)

You'll want to create a nice profile on one or more of these sites. Make sure you fill out all the profile information, including a picture of yourself, and a good description of the services you provide.

Ideally, you'll also want to create a brochure site for yourself (using WordPress and a nice theme is a good idea). Have your own chatbot to showcase your skills and to actually provide answers to prospective customers.

Finally, as your chatbot skills grow and you begin to offer integration with external data sources and service APIs, you'll be able to create more complex and useful chatbots, and consequently, demand more premium fees.
