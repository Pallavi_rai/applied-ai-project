## Reading: What's Next for AI

In Q&A sessions, IBM gathered the opinions of AI visionaries to consider what the future might hold for AI.

_Click each name to discover more, and then return to the course to continue_.

[Kevin Kelly, Tech Author and co-founder of Wired](https://www.ibm.com/watson/advantage-reports/future-of-artificial-intelligence/kevin-kelly.html)

Kevin is optimistic about the effect of AI on jobs, and believes that a key role of AI will be to transform big data into something businesses can use.

[Mark Sagar, Oscar-winning AI engineer](https://www.ibm.com/watson/advantage-reports/future-of-artificial-intelligence/mark-sagar.html)

Mark combines his knowledge of computer graphics, human physiology and artificial intelligence to develop emotionally responsive avatars for businesses.

[Chieko Asakawa, Accessibility research pioneer & IBM Fellow](https://www.ibm.com/watson/advantage-reports/future-of-artificial-intelligence/chieko-asakawa.html)

One of the world’s premier accessibility researchers, Chieko is excited about the progress of Ai in helping blind people experience and explore the world.

[Yoshua Bengio, Preeminent deep learning researcher](https://www.ibm.com/watson/advantage-reports/future-of-artificial-intelligence/yoshua-bengio.html)

Describe as a founding father of deep learning, Yoshua sees unsupervised learning and natural language processing as being the areas in which AI will develop very quickly.

[Margaret Boden, Veteran AI & cognitive scientist](https://www.ibm.com/watson/advantage-reports/future-of-artificial-intelligence/margaret-boden.html)

Margaret is interested in how AI will help us understand human creativity, and champions a multidisciplinary approach to AI.
