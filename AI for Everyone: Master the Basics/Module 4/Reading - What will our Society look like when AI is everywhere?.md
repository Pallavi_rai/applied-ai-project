## Reading: What will our Society look like when AI is everywhere?

_Writing for the Smithsonian magazine, Stephan Talty and Jules Julien paint a fiction picture of how life might be in a society where AI is almost everywhere. The full article can be read here: [What Will Our Society Look Like When Artificial Intelligence Is Everywhere?](https://www.smithsonianmag.com/innovation/artificial-intelligence-future-scenarios-180968403/)_

AI has come a long way since the seminal “Dartmouth workshop” at Dartmouth college in June 1956. A new discipline was being discussed, so new that “People didn’t agree on what it was, how to do it or even what to call it,” said Grace Solomonoff, widow of one of the workshop attendees. In recent decades, AI has developed to be present in our everyday lives, to an extent that has caused some to wonder: how far can this go, and what will happen then?

Read the article and then return to continue the course.
